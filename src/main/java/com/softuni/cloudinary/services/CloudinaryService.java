package com.softuni.cloudinary.services;

import com.softuni.cloudinary.models.CloudinaryImageModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CloudinaryService {

    CloudinaryImageModel uploadImage(MultipartFile image) throws IOException;

    boolean delete(String publicId);
}
