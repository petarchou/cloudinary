package com.softuni.cloudinary.services;

import com.cloudinary.Cloudinary;
import com.softuni.cloudinary.models.CloudinaryImageModel;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Service
public class CloudinaryServiceImpl implements CloudinaryService {

    private static final String TEMP_FILE = "temp-file";
    private static final String URL = "url";
    private static final String PUBLIC_ID = "public_id";
    private final Cloudinary cloudinary;

    public CloudinaryServiceImpl(Cloudinary cloudinary) {
        this.cloudinary = cloudinary;
    }


    @Override
    public CloudinaryImageModel uploadImage(MultipartFile image) throws IOException {

        File tempFile = File.createTempFile(TEMP_FILE, image.getOriginalFilename());
        image.transferTo(tempFile);

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> uploadResult = cloudinary.uploader()
                    .upload(tempFile, Map.of());

            String url = uploadResult.getOrDefault(URL,"https://img.freepik.com/premium-vector/modern-minimal-404-error-page-website-404-error-page-found-with-dead-ghost-concept_599740-702.jpg?w=2000");
            String publicId = uploadResult.getOrDefault(PUBLIC_ID,"");

            return new CloudinaryImageModel().setUrl(url).setPublicId(publicId);
        }
        finally {
            tempFile.delete();
        }

    }

    @Override
    public boolean delete(String publicId) {
        try{
            cloudinary.uploader()
                    .destroy(publicId,Map.of());
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
