package com.softuni.cloudinary.config;

import com.cloudinary.Cloudinary;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.Map;

@Configuration
@PropertySource("classpath:application.yml")
public class AppConfig {

    private final CloudinaryConfig cloudinaryConfig;

    private final String cloudName;
    private final String apiKey;
    private final String apiSecret;

    public AppConfig(CloudinaryConfig cloudinaryConfig) {
        this.cloudinaryConfig = cloudinaryConfig;
        this.cloudName = cloudinaryConfig.getCloudName();
        this.apiKey = cloudinaryConfig.getApiKey();
        this.apiSecret = cloudinaryConfig.getApiSecret();
    }

    @Bean
    public Cloudinary cloudinary() {
        return new Cloudinary(Map.of("cloud_name",cloudName,
                "api_key",apiKey,
                "api_secret",apiSecret));
    }
}
