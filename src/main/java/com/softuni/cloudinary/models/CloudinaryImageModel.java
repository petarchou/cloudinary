package com.softuni.cloudinary.models;

public class CloudinaryImageModel {

    private String publicId;
    private String url;


    public String getPublicId() {
        return publicId;
    }

    public CloudinaryImageModel setPublicId(String publicId) {
        this.publicId = publicId;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public CloudinaryImageModel setUrl(String url) {
        this.url = url;
        return this;
    }
}
