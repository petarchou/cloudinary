package com.softuni.cloudinary.models.binding;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.bind.ConstructorBinding;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
public class PictureBindingModel {

    private String title;
    private MultipartFile picture;


    public String getTitle() {
        return title;
    }

    public MultipartFile getPicture() {
        return picture;
    }

    public PictureBindingModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public PictureBindingModel setPicture(MultipartFile picture) {
        this.picture = picture;
        return this;
    }
}
