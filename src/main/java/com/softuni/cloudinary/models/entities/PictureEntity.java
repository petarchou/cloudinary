package com.softuni.cloudinary.models.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="pictures")
public class PictureEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String url;
    private String publicId;


    public PictureEntity setId(long id) {
        this.id = id;
        return this;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getPublicId() {
        return publicId;
    }

    public PictureEntity setTitle(String title) {
        this.title = title;
        return this;
    }

    public PictureEntity setUrl(String url) {
        this.url = url;
        return this;
    }

    public PictureEntity setPublicId(String publicId) {
        this.publicId = publicId;
        return this;
    }
}
