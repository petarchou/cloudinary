package com.softuni.cloudinary.controllers;

import com.softuni.cloudinary.models.CloudinaryImageModel;
import com.softuni.cloudinary.models.binding.PictureBindingModel;
import com.softuni.cloudinary.models.entities.PictureEntity;
import com.softuni.cloudinary.repositories.PictureRepository;
import com.softuni.cloudinary.services.CloudinaryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class PictureController {

    private final CloudinaryService cloudinaryService;

    private final PictureRepository pictureRepository;

    public PictureController(CloudinaryService cloudinaryService, PictureRepository pictureRepository) {
        this.cloudinaryService = cloudinaryService;
        //DON'T ADD REPO DIRECTLY - This is for convenience of testing
        this.pictureRepository = pictureRepository;
    }


    @GetMapping("/pictures/add")
    public String addPictureView() {
        return "add";
    }

    @PostMapping("/pictures/add")
    public String addPicture(PictureBindingModel bindingModel) throws IOException {

        CloudinaryImageModel uploadResult = cloudinaryService.uploadImage(bindingModel.getPicture());


        PictureEntity picture = new PictureEntity()
                .setTitle(bindingModel.getTitle())
                .setUrl(uploadResult.getUrl())
                .setPublicId(uploadResult.getPublicId());
        pictureRepository.saveAndFlush(picture);

        return "redirect:/pictures/all";
    }

    @GetMapping("/pictures/all")
    public String getAll(Model model){

        List<PictureEntity> pictures = pictureRepository.findAll();
        model.addAttribute("pictures",pictures);

        return "all";
    }
}
