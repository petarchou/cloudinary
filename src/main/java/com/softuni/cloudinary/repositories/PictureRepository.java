package com.softuni.cloudinary.repositories;

import com.softuni.cloudinary.models.entities.PictureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PictureRepository extends JpaRepository<PictureEntity, Long> {
}
